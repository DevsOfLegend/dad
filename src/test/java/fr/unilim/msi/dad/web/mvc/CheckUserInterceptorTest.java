package fr.unilim.msi.dad.web.mvc;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;


/***************************************************************************//**
 * @brief Les JTests pour CheckUserInterceptor
 */
public class CheckUserInterceptorTest
{
    private CheckUserInterceptor interceptor;
    private UserSession userSession;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;


    @Before
    public void createInstances()
	{
        interceptor = new CheckUserInterceptor();
        userSession = new UserSession();
        interceptor.setUserSession(userSession);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }


    @Test
    public void checkPreHandleServletPathIsLogin() throws IOException
	{
        request.setServletPath("/login.htm");
        assertTrue(interceptor.preHandle(request, response, null));
    }


    @Test
    public void checkPreHandleUsernameInSession() throws IOException
	{
        userSession.setUsername("admin");
        request.setServletPath("/blabla.htm");
        assertTrue(interceptor.preHandle(request, response, null));
    }


    @Test
    public void checkPreHandleUsernameNotInSession() throws IOException
	{
        request.setServletPath("/blabla.htm");
        assertFalse(interceptor.preHandle(request, response, null));
        assertEquals(response.getRedirectedUrl(),"login.htm");
    }
}
