package fr.unilim.msi.dad.web.mvc;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


/***************************************************************************//**
 * @brief Les JTests pour FriendDao
 */
public class FriendDaoTest
{
    private FriendDao friendDao;
	private User a;
	private User b;
	private User c;

    @Before
    public void createInstances()
	{
        friendDao = new FriendDaoImpl();
		a = new User().set("u1", "pass");
		b = new User().set("u2", "pass");
		c = new User().set("u3", "pass");
    }

	@Test
	public void test_SendInvitation()
	{
		assertFalse(friendDao.sendInvitation(a, a));
		assertTrue(friendDao.sendInvitation(a, b));
		assertFalse(friendDao.sendInvitation(a, b));
		assertTrue(friendDao.sendInvitation(b, a));
	}


	@Test
	public void test_invitationAccepted()
	{
		assertTrue(friendDao.sendInvitation(a, b));
		assertTrue(friendDao.invitationAccepted(a, b));

		assertFalse(friendDao.invitationAccepted(b, a));
	}


	@Test
	public void test_getFriendList()
	{
		assertEquals(friendDao.getFriendList(a).size(), 0);

		assertTrue(friendDao.sendInvitation(a, b));
		assertTrue(friendDao.invitationAccepted(a, b));

		assertEquals(friendDao.getFriendList(a).size(), 1);
		assertEquals(friendDao.getFriendList(b).size(), 1);

		assertTrue(friendDao.getFriendList(a).contains(b));
	}


	@Test
	public void test_getInvitationSendBy()
	{
		assertEquals(friendDao.getInvitationSendBy(a).size(), 0);

		assertTrue(friendDao.sendInvitation(a, b));

		assertEquals(friendDao.getInvitationSendBy(a).size(), 1);
		assertEquals(friendDao.getInvitationSendBy(b).size(), 0);

		assertTrue(friendDao.invitationAccepted(a, b));

		assertEquals(friendDao.getInvitationSendBy(a).size(), 0);
		assertEquals(friendDao.getInvitationSendBy(b).size(), 0);
	}


	@Test
	public void test_getInvitationSendFor()
	{
		assertEquals(friendDao.getInvitationSendFor(a).size(), 0);

		assertTrue(friendDao.sendInvitation(a, b));

		assertEquals(friendDao.getInvitationSendFor(a).size(), 0);
		assertEquals(friendDao.getInvitationSendFor(b).size(), 1);

		assertTrue(friendDao.invitationAccepted(a, b));

		assertEquals(friendDao.getInvitationSendFor(a).size(), 0);
		assertEquals(friendDao.getInvitationSendFor(b).size(), 0);
	}


	@Test
	public void test_isFriend()
	{
		assertFalse(friendDao.isFriend(a, b));
		assertTrue(friendDao.sendInvitation(a, b));
		assertFalse(friendDao.isFriend(a, b));
		assertTrue(friendDao.invitationAccepted(a, b));
		assertTrue(friendDao.isFriend(a, b));
		assertTrue(friendDao.isFriend(b, a));
	}


	@Test
	public void test_remove()
	{
		assertFalse(friendDao.remove(a, b));

		assertTrue(friendDao.sendInvitation(a, b));
		assertTrue(friendDao.sendInvitation(a, c));

		assertTrue(friendDao.remove(c, a));
		assertTrue(friendDao.remove(a, b));
		
		assertFalse(friendDao.remove(a, b));
	}


	@Test
	public void test_size()
	{
		assertEquals(friendDao.size(), 0);
		assertTrue(friendDao.sendInvitation(a, b));
		assertEquals(friendDao.size(), 1);
	}


	@Test
	public void test_ConnectionFriend_Struct()
	{
		FriendDaoImpl fd = new FriendDaoImpl();
		// Test hashCode
		assertEquals(fd.structTest_hashCode(a, b), a.hashCode()+b.hashCode()*7);

		// Test Equals
		assertTrue(fd.structTest_equals(a, b, a, b));
		assertFalse(fd.structTest_equals(a, b, b, b));
		assertFalse(fd.structTest_equals(a, b, a, a));

		assertFalse(fd.structTest_equals(a, b, null, null));
	}


	@Test
	public void test_SendAndAccept()
	{
		// Envoie d'une invitation
		assertTrue(friendDao.sendInvitation(new User().set("a","aaa"), new User().set("b", "bbb")));

		// Le nombre de relation dans la bd doit être = à 1
		assertTrue(friendDao.size()==1);

		// On check que l'invitation est bien arrivée
		assertEquals(friendDao.getInvitationSendBy(new User().set("a",null)).size(), 1);
		assertEquals(friendDao.getInvitationSendFor(new User().set("b",null)).size(), 1);

		// On check que l'invitation n'est pas dans l'autre sens
		assertEquals(friendDao.getInvitationSendBy(new User().set("b",null)).size(), 0);
		assertEquals(friendDao.getInvitationSendFor(new User().set("a",null)).size(), 0);

		// On confirme l'invitation
		assertTrue(friendDao.invitationAccepted(new User().set("a",null), new User().set("b", null)));
		// On check que l'on est bien ami dans les 2 sens
		assertEquals(friendDao.getFriendList(new User().set("a",null)).size(), 1);
		assertTrue(friendDao.getFriendList(new User().set("a",null)).contains(new User().set("b", null)));
		assertEquals(friendDao.getFriendList(new User().set("b",null)).size(), 1);
		assertTrue(friendDao.getFriendList(new User().set("b",null)).contains(new User().set("a", null)));

		// On check que l'on n'est plus dans les invitation en attentes
		assertEquals(friendDao.getInvitationSendBy(new User().set("a",null)).size(), 0);
		assertEquals(friendDao.getInvitationSendFor(new User().set("b",null)).size(), 0);
		assertEquals(friendDao.getInvitationSendBy(new User().set("b",null)).size(), 0);
		assertEquals(friendDao.getInvitationSendFor(new User().set("a",null)).size(), 0);

		// On test que l'on ne peut pas envoyer une invitation a soit même
		assertFalse(friendDao.invitationAccepted(new User().set("a",null), new User().set("a", "rrr")));

		// Le nombre de relation dans la bd doit être = à 1
		assertTrue(friendDao.size()==1);
	}
}
