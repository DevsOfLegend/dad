package fr.unilim.msi.dad.web.mvc;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.ModelMap;


/***************************************************************************//**
 * @brief Les JTests pour ViewUsersController
 */
public class ViewUsersControllerTest
{

    private ViewUsersController viewUsersController;
    private UserDao userDao;
    private UserSession userSession;
	private FriendDao fd;
	private User userAdm;
	private User userA;
	private User userB;

    @Before
    public void createInstances()
	{
        viewUsersController = new ViewUsersController();
        userDao = new UserDaoImpl();
        viewUsersController.setUserDao(userDao);
        userSession = new UserSession();
        userSession.setUsername("admin");
        viewUsersController.setUserSession(userSession);
		fd = new FriendDaoImpl();
		viewUsersController.setFriendDao(fd);

		userAdm = new User().set("admin", "pass");
		userA = new User().set("userA", "pass");
		userB = new User().set("userB", "pass");
    }


    @Test
    public void test_PopulateUsers()
	{
        assertEquals(2, viewUsersController.populateUsers().size());
    }


    @Test
    public void test_PopulateUser()
	{
        assertEquals("admin", viewUsersController.populateUser().getUsername());
    }


	@Test
	public void test_populateFriends()
	{
		assertEquals(viewUsersController.populateFriends().size(), 0);
		fd.sendInvitation(userAdm, userA);
		assertEquals(viewUsersController.populateFriends().size(), 0);
		fd.invitationAccepted(userAdm, userA);
		assertEquals(viewUsersController.populateFriends().size(), 1);
	}


    @Test
    public void test_GetViewUsers()
	{
        assertEquals("viewUsers",viewUsersController.getViewUsers());
    }


	@Test
	public void test_acceptInvitation()
	{
		ModelMap mm = new ModelMap();
		assertEquals(viewUsersController.acceptInvitation(mm, "userNotInDataBase"), "redirect:/index.htm");
		assertEquals(mm.get("ViewUsersController.acceptInvitation"), "notFound");

		mm = new ModelMap();
		userDao.save(userA);
		fd.sendInvitation(userA, userAdm);
		assertEquals(viewUsersController.acceptInvitation(mm, userA.getUsername()), "redirect:/index.htm");
		assertEquals(mm.get("ViewUsersController.acceptInvitation"), "OK");

		mm = new ModelMap();
		fd = new FriendDaoImpl();
		viewUsersController.setFriendDao(fd);
		assertEquals(viewUsersController.acceptInvitation(mm, userA.getUsername()), "redirect:/index.htm");
		assertEquals("noRelationFound", mm.get("ViewUsersController.acceptInvitation"));
	}


	@Test
	public void test_removeFriend()
	{
		ModelMap mm = new ModelMap();
		assertEquals(viewUsersController.removeFriend(mm, "userNotInDataBase"), "redirect:/index.htm");
		assertEquals(mm.get("ViewUsersController.removeFriend"), "notFound");

		mm = new ModelMap();
		userDao.save(userA);
		fd.sendInvitation(userAdm, userA);
		fd.invitationAccepted(userAdm, userA);
		assertEquals(viewUsersController.removeFriend(mm, userA.getUsername()), "redirect:/index.htm");
		assertEquals(mm.get("ViewUsersController.removeFriend"), "OK");

		mm = new ModelMap();
		assertEquals(viewUsersController.removeFriend(mm, userA.getUsername()), "redirect:/index.htm");
		assertEquals(mm.get("ViewUsersController.removeFriend"), "noRelationFound");
	}


	@Test
	public void test_setterAndGetter()
	{
		viewUsersController.setUserDao(userDao);
		assertEquals(viewUsersController.getUserDao(), userDao);

		viewUsersController.setFriendDao(fd);
		assertEquals(viewUsersController.getFriendDao(), fd);

		viewUsersController.setUserSession(userSession);
		assertEquals(viewUsersController.getUserSession(), userSession);
	}


	@Test
	public void testPopulate()
	{
		// On test ques les invitations sont vides
		assertTrue(viewUsersController.populateInvitationsSent().isEmpty());
		assertTrue(viewUsersController.populateInvitationsRcv().isEmpty());
		assertTrue(viewUsersController.populateFriends().isEmpty());

		// Création d'une invitation
		fd.sendInvitation(new User().set("admin", null), new User().set("toto",null));
		assertTrue(!viewUsersController.populateInvitationsSent().isEmpty());
		assertTrue(viewUsersController.populateInvitationsRcv().isEmpty());
		assertTrue(viewUsersController.populateFriends().isEmpty());

		userSession.setUsername("toto");
		assertTrue(viewUsersController.populateInvitationsSent().isEmpty());
		assertTrue(!viewUsersController.populateInvitationsRcv().isEmpty());
		assertTrue(viewUsersController.populateFriends().isEmpty());

		fd.invitationAccepted(new User().set("admin", null), new User().set("toto",null));

		assertTrue(viewUsersController.populateUser().getUsername().equals("toto"));
	}
}
