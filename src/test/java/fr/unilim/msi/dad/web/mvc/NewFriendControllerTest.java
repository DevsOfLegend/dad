package fr.unilim.msi.dad.web.mvc;

import java.beans.PropertyEditor;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;


/***************************************************************************//**
 * @brief Les JTests pour NewFriendController
 */
public class NewFriendControllerTest
{
	private NewFriendController nc;
	private FriendDao fd;
	private UserDao ud;
	private UserSession us;
	private User userA;

	@Before
	public void createInstances()
	{
		nc = new NewFriendController();

		ud = new UserDaoImpl();
		nc.setUserDao(ud);

		fd = new FriendDaoImpl();
		nc.setFriendDao(fd);

		us = new UserSession();
		us.setUsername("admin");
		nc.setUserSession(us);

		userA = new User().set("userA", "pass");
	}


	@Test
	public void test_getNewFriend()
	{
		assertEquals("newFriend", nc.getNewFriend().getViewName());
	}


	@Test
	public void test_postNewUser()
	{
		ModelMap mm;
		BindingResult br;

		// Préparation if 1
		mm = new ModelMap();
		br = new BeanPropertyBindingResult(userA,userA.getUsername());

		// if 1
		assertEquals("newFriend", nc.postNewUser(mm, userA, br));
		assertTrue(br.getAllErrors().get(0).toString().contains("new.friend.not.found"));


		// Préparation if 2
		mm = new ModelMap();
		br = new BeanPropertyBindingResult(userA,userA.getUsername());
		ud.save(userA);

		// if 2
		assertEquals("redirect:/newFriend.htm", nc.postNewUser(mm, userA, br));
		assertEquals(userA.getUsername(), mm.get("NewFriendController-invitationSent"));

		// if 3
		assertEquals("redirect:/newFriend.htm", nc.postNewUser(mm, userA, br));
		assertEquals(1, mm.get("NewFriendController-invitationSent-Fail"));

		fd.invitationAccepted(new User().set("admin", "pass"), userA);

		// if 4
		assertEquals("redirect:/newFriend.htm", nc.postNewUser(mm, userA, br));
		assertEquals(1, mm.get("NewFriendController-invitationSent-Fail-AlreadyExist"));
	}
}
