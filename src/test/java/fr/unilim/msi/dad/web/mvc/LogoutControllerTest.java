package fr.unilim.msi.dad.web.mvc;

import static org.junit.Assert.*;
import org.junit.Test;


/***************************************************************************//**
 * @brief Les JTests pour LogoutController
 */
public class LogoutControllerTest
{
    @Test
    public void testGetLogout()
	{
        LogoutController logoutController = new LogoutController();
        UserSession userSession = new UserSession();
        userSession.setUsername("blabla");
        logoutController.setUserSession(userSession);
        String view = logoutController.getLogout();
        assertNull(userSession.getUsername());
        assertEquals("redirect:/index.htm",view);
    }
}
