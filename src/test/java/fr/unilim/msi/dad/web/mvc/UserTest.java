/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unilim.msi.dad.web.mvc;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/***************************************************************************//**
 * @brief Les JTests pour User
 */
public class UserTest
{
	private User u;


	@Before
	public void init()
	{
		u = new User();
	}


	@Test
	public void setter_getter()
	{
		// Test du nom d'utilisateur
		assertNull(u.getUsername());
		u.setUsername("Toto");
		assertEquals(u.getUsername(), "Toto");

		// Test du mot de passe
		assertNull(u.getPassword());
		u.setPassword("Toto");
		assertEquals(u.getPassword(), "Toto");

		assertSame(u.set("user", "pass"), u);
		assertEquals(u.getUsername(), "user");
		assertEquals(u.getPassword(), "pass");
	}


	@Test
	public void test_equals()
	{
		User b = new User();
		b.set("user", "pass");

		u.set("user1","pass");

		assertFalse(b.equals(null));

		assertFalse(b.equals(new String()));

		assertFalse(b.equals(u));

		u.set("user","pass");

		assertTrue(b.equals(u));
	}


	@Test
	public void Test_HashCode()
	{
		assertEquals(u.hashCode(), 29*7);

		u.setUsername("toto");

		assertEquals(u.hashCode(), 29*7+"toto".hashCode());
	}
}
