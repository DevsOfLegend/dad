<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><spring:message code="view.users.page.title"/></title>
		<style type="text/css">
			#menu {
				width : 120px;
				float : left;
				font-weight : bolder;
				padding-top : 5px;
				padding-bottom : 5px;
				margin-bottom : 10px;
				margin-left : 15px;
				margin-right : 15px;
				color : black;
				text-decoration : none;
			}
			#menu li {
				margin : 5px;
				list-style: none;
			}
			#menu a{
				text-decoration: none;
				border-bottom: 1px dotted black;
				color : black;
			}
			#menu a:hover{
				position : relative;
				border-bottom: 1px dashed black;
				color: blue;
			}
			body {
				width: 1024px;
				margin-left: auto;
				margin-right: auto;
				border: 1px solid transparent;
				-webkit-border-radius: 10px 0px 0px 0px;
				border-radius: 10px 0px 0px 0px;
				background-color: rgba(116, 207, 241, 0.2);
				/* Ombre */
				-webkit-box-shadow: 3px 3px 10px 2px rgba(0, 0, 0, 0.3);
				-moz-box-shadow: 3px 3px 10px 2px rgba(0, 0, 0, 0.3);
				box-shadow: 3px 3px 10px 2px rgba(0, 0, 0, 0.3);
			}
			#corps {
				margin-bottom:50px;
				background-color: white;
			}
			table {
				/* Bordures */
				-webkit-border-radius:10px / 10px;
				-moz-border-radius:10px / 10px;
				-o-border-radius:10px / 10px;
				border-radius:10px / 10px;
				/* Ombre */
				-webkit-box-shadow: 3px 3px 10px 2px rgba(127, 171, 188, 0.4);
				-moz-box-shadow: 3px 3px 10px 2px rgba(127, 171, 188, 0.4);
				box-shadow: 3px 3px 10px 2px rgba(127, 171, 188, 0.4);
				background-color: rgba(116, 207, 241, 0.5);
				padding: 5px;
			}
			td {
				border-bottom: 1px solid #7fabbc;
				padding : 20px;
			}
			table tr td:first-child {
				font-weight: bolder;
				vertical-align:top;
			}
			h1 {
				font-family : "time new roman";
				text-align : center;
				font-weight : bold;
			}
			#info {
				margin-left : 300px;
			}
		</style>

    </head>
    <body>
        <h1><spring:message code="view.users.greetings"/> ${userSession.username} !</h1>

		<div style="text-align:center; font-weight: bold;"><%
		request.getSession().toString();

		String req = (String)request.getSession().getAttribute("ViewUsersController.acceptInvitation");
		if( req != null ){
			if( req.equals("OK") ){
				%><div style="color:green;"><spring:message code="invitation.ok"/></div><%
			}else if( req.equals("notFound") ){
				%><div style="color:red;"><spring:message code="invitation.notFound"/></div><%
			}else if( req.equals("noRelationFound") ){
				%><div style="color:red;"><spring:message code="invitation.noRelationFound"/></div><%
			}
			request.getSession().removeAttribute("ViewUsersController.acceptInvitation");
		}

		String req_rmv = (String)request.getSession().getAttribute("ViewUsersController.removeFriend");
		if( req_rmv != null ){
			if( req_rmv.equals("OK") ){
				%><div style="color:green;"><spring:message code="remove.ok"/></div><%
			}else if( req_rmv.equals("notFound") ){
				%><div style="color:red;"><spring:message code="remove.friend.notFound"/></div><%
			}else if( req_rmv.equals("noRelationFound") ){
				%><div style="color:red;"><spring:message code="remove.friend.noRelationFound"/></div><%
			}
			request.getSession().removeAttribute("ViewUsersController.removeFriend");
		}

		%></div>
		<div id="corps">
			<ul id="menu">
				<h3>MENU</h3>
				<!----------------------------------------------------------------------
				-- les trois lignes suivantes permettent de :
				--	LIGNE n°1 -> jumper sur une page permettant la création d'un nouvel utilisateur
				--	LIGNE n°2 -> jumper sur une page permettant d'ajouter un nouvel ami
				--	LIGNE n°3 -> jumper sur une page permettant d'accepter les invitations
				--	LIGNE n°4 -> se déconnecter
				-->
				<li><a href="newUser.htm"><spring:message code="new.user.main.header"/></a></li>
				<li><a href="newFriend.htm"><spring:message code="new.friend.header"/></a></li>
				<li><a href="logout.htm"><spring:message code="view.users.main.logout"/></a></li>
			</ul>

			<table id="info">
				<tr>
					<td style="text-align: right; padding-right : 10px;"><spring:message code="all.users"/></td>
					<td>
						<c:forEach items="${users}" var="user">
							${user.username}<br />
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td style="text-align: right; padding-right : 10px;"><spring:message code="all.friends"/></td>
					<td>
						<c:if test="${friends.size() == 0}">
							<p style="color:green; font-weight: bolder">No friend... :'(</p>
						</c:if>
						<c:if test="${friends.size() != 0}">
							<table>
								<c:forEach items="${friends}" var="friend">
								<tr>
									<td>${friend.username}<br /></td>
									<td><input type="button" value="<spring:message code="delete"/>" onclick="document.location.href='./delete-${friend.username}.htm';" /></td>
								</tr>
								</c:forEach>
							</table>
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="text-align: right; padding-right : 10px;"><spring:message code="invitation.sent"/></td>
					<td>
						<c:if test="${invitationsTo.size() == 0}">
							<p style="color:green; font-weight: bolder">No waiting invitation...</p>
						</c:if>
						<c:forEach items="${invitationsTo}" var="iv">
							${iv.username}<br />
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td style="text-align: right; padding-right : 10px;"><spring:message code="invitation.received"/></td>
					<td>
						<c:if test="${invitationsRcv.size() == 0}">
							<p style="color:green; font-weight: bolder">No invitation for you...</p>
						</c:if>
						<c:forEach items="${invitationsRcv}" var="iv">
							<a href="accept-${iv.username}.htm">${iv.username}</a><br />
						</c:forEach>
					</td>
				</tr>
			</table>
		</div> <!--fin corps.-->
	</body>
</html>
