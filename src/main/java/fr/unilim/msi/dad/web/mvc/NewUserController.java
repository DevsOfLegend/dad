package fr.unilim.msi.dad.web.mvc;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/***************************************************************************//**
 * @brief Classe contrôlant la validité des champs choisi par un nouvel utilisateur
 *
 * @note
 *	-> Les login/password doivent dépasser les 3 lettres
 *	-> Le login doit être unique (s'il existe déjà un vide le formulaire,
 *		l'utilisateur doit saisir un autre login.
 */
@Controller
public class NewUserController
{
	@Resource
	private UserDao m_userDao;//!< Système de gestion des utilisateurs
	@Resource
	private UserSession m_userSession;//!< Session de l'utilisateur courant
	@Resource
	private UserValidator m_userValidator;//!< correspond à la validité du mot de passe (longueur password/login >3)


	/***********************************************************************//**
	 * @brief Permet de créer l'association entre le lien et la page JSP correspondante
	 * @return ModelAndView	La vue a utiliser
	 *
	 */
	@RequestMapping(value = {"/newUser.htm"}, method = RequestMethod.GET)
	public ModelAndView getNewUserForm()
	{
		return new ModelAndView("newUser", "user", new User());
	}


	/***********************************************************************//**
	 * @brief Traitement POST remplissage de formulaire
	 * @param[in] user			Utilisateur en phase d'inscription
	 * @param[in,out] result	Stocke le résultat du traitement
	 * @return La vue a utiliser / Une redirection
	 *
	 */
	@RequestMapping(value = {"/newUser.htm"}, method = RequestMethod.POST)
	public String postNewUser( User user, BindingResult result )
	{
		assert user != null : "{user} ne peut être null !";
		assert result != null : "{result} ne peut être null !";

		if( !m_userSession.getUsername().equals("admin") )
			result.rejectValue("username", "new.user.only.admin");

		m_userValidator.validate(user, result);

		if( m_userDao.getFromUsername(user.getUsername()) != null )
			result.rejectValue("username", "new.user.form.present");

		if( result.hasErrors() )
			return "newUser";

		m_userDao.save(user);

		return "redirect:/index.htm";
	}


	/***********************************************************************//**
	 * @brief Initialisation de l'ensemble des utilisateurs
	 * @param[in,out] userDao	Ensemble des utilisateurs
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserDao( UserDao userDao )
	{
		m_userDao = userDao;
	}


	/***********************************************************************//**
	 * @brief Initialisation de la session en cours avec l'utilisateur en cours
	 * @param[in,out] userSession	Contient les informations de la session en cours
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}


	/***********************************************************************//**
	 * @brief Initialisation de la vérification de validité d'un nouvel utilisateur
	 * @param[in,out] userValidator	Le validateur a utiliser
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserValidator( UserValidator userValidator )
	{
		m_userValidator = userValidator;
	}
}
