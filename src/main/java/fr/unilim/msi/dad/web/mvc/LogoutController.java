package fr.unilim.msi.dad.web.mvc;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/***************************************************************************//**
 * @brief Cette class permet de gérer le système de déconnection ( logout )
 */
@Controller
public class LogoutController
{
	@Resource
	private UserSession m_userSession;//!< Le user actuellement logué


	/***********************************************************************//**
	 * @brief Cette fonction se charge de déconnecter l'utilisateur puis le
	 * redirige vers l'index.
	 * @return La vue a utiliser / Une redirection
	 */
	@RequestMapping(value = {"/logout.htm"}, method = RequestMethod.GET)
	public String getLogout()
	{
		m_userSession.setUsername(null);
		return "redirect:/index.htm";
	}


	/***********************************************************************//**
	 * @brief Permet de definir le userSession
	 * @param userSession	Le nouveau userSession
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}
}
