package fr.unilim.msi.dad.web.mvc;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;


/***************************************************************************//**
 * @brief Classe contrôlant la validité d'une demande de relation d'amitié
 *
 * @note Le destinataire de la demande de relation doit faire parti des utilisateurs
 * existants.
 */
@Controller
@SessionAttributes({"NewFriendController-invitationSent","NewFriendController-invitationSent-Fail","NewFriendController-invitationSent-Fail-AlreadyExist"})
public class NewFriendController
{
	@Resource
	private UserDao m_userDao;//!< Système de gestion des utilisateurs
	@Resource
	private FriendDao m_friendDao;//!< Système de gestion des amis
	@Resource
	private UserSession m_userSession;//!< Session courante


	/***********************************************************************//**
	 * @brief Permet de faire l'association entre le le fichier HTM et le fichier
	 * JSP correspondant.
	 * @return La vue a utiliser
	 *
	 */
	@RequestMapping(value = {"/newFriend.htm"}, method = RequestMethod.GET)
	public ModelAndView getNewFriend()
	{
		return new ModelAndView("newFriend", "user", new User());
	}


	/***********************************************************************//**
	 * @brief permet de traiter la demande de relation
	 * @param[in,out] model		Modelmapping pour l'ajout de variable.
	 * @param[in] u				destinataire de la demande de relation
	 * @param[in,out] result	Stocke le résultat du traitement
	 * @return La vue a utiliser / Une redirection
	 *
	 */
	@RequestMapping(value = {"/newFriend.htm"}, method = RequestMethod.POST)
	public String postNewUser( ModelMap model, User u, BindingResult result )
	{
		assert model != null : "{model} ne peut être null !";
		assert u != null : "{u} ne peut être null !";
		assert result != null : "{result} ne peut être null !";

		// On teste si l'utilisateur,à qui on fait la demande d'ajout à ma liste d'amis, existe
		// Si la "proposition d'ajout à ma liste d'amis" concerne un utilisateur non existant :
		if( m_userDao.getFromUsername(u.getUsername()) == null ) {
			// Je retourne une erreur : utilisateur inexistant.
			result.rejectValue("username", "new.friend.not.found");
			return "newFriend";
		}

		// Sinon j'envoie une invitation à l'utilisateur
		User sessionUser = m_userDao.getFromUsername(m_userSession.getUsername());
		User friendToAdd = m_userDao.getFromUsername(u.getUsername());

		//il n'existe pas de relation d'amitié entre les utilisateurs
		if( !m_friendDao.isFriend(u, new User().set(m_userSession.getUsername(),null)) ){
			if( m_friendDao.sendInvitation(sessionUser, friendToAdd) ){
				model.put("NewFriendController-invitationSent", u.getUsername());
			}else{
				model.put("NewFriendController-invitationSent-Fail", 1);
			}
		//il existe déjà une relation d'amitié entre les deux utilisateurs
		}else {
			model.put("NewFriendController-invitationSent-Fail-AlreadyExist", 1);
		}


		// Redirection vers la page d'accueil de l'utilisateur
		return "redirect:/newFriend.htm";
	}


	/***********************************************************************//**
	 * @brief Initialisation de l'ensemble des utilisateurs
	 * @param[in,out] userDao ensemble des utilisateurs
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserDao( UserDao userDao )
	{
		m_userDao = userDao;
	}


	/***********************************************************************//**
	 * @brief Initialisation de l'ensemble des amis
	 * @param[in,out] friendDao	Ensemble des amis
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setFriendDao( FriendDao friendDao )
	{
		m_friendDao = friendDao;
	}


	/***********************************************************************//**
	 * @brief Initialisation de la session en cours avec l'utilisateur en cours
	 * @param[in,out] userSession	Contient les informations de la session en cours
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}
}
