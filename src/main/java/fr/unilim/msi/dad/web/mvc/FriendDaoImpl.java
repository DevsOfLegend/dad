/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.unilim.msi.dad.web.mvc;

import java.util.HashSet;
import java.util.Set;
import org.springframework.stereotype.Repository;


/***************************************************************************//**
 * @brief Cette classe permet de gérer une liste d'ami.
 */
@Repository(value = "friendDao")
public class FriendDaoImpl implements FriendDao
{
	/***********************************************************************//**
	 * @brief Cette classe permet de manipuler aisement une relation entre amis.
	 *
	 * Cette classe n'a pas pour vocation d'être utilisée en dehors de la class
	 * FriendDaoImpl.
	 */
	private static class ConnectionFriend
	{
		public User	m_from;//!< Invitation de la part de {from}
		public User m_to;//!< Invitation adressé à {to}
		public boolean m_isAccepted = false;//!< La relation a t'elle été accèptée par {to}


		/*******************************************************************//**
		 * @brief Constructeur. Permet de créer une invitation pour une relation.
		 * @param[in] from	Invitation de la part de ....
		 * @param[in] to	Invitation pour ...
		 * @return [NONE]
		 */
		public ConnectionFriend( User from, User to )
		{
			m_from = from;
			m_to = to;
		}


		/*******************************************************************//**
		 * @brief Fonction de hachage pour Set<Object>
		 * @return Valeur numéric représantant la relation.
		 */
		@Override
		public int hashCode()
		{
			return m_from.hashCode() + m_to.hashCode()*7;
		}


		/*******************************************************************//**
		 * @brief Comparapteur pour Set<>
		 * @return TRUE si from et to des 2 objets sont identiques. FALSE sinon
		 */
		@Override
		public boolean equals( Object obj )
		{
			if( obj == null || getClass() != obj.getClass() )
				return false;

			final ConnectionFriend other = (ConnectionFriend) obj;
			if( this.m_from != other.m_from && (this.m_from == null || !this.m_from.equals(other.m_from)) )
				return false;

			if( this.m_to != other.m_to && (this.m_to == null || !this.m_to.equals(other.m_to)) )
				return false;
			return true;
		}
	}


	private Set<ConnectionFriend> m_users = new HashSet<ConnectionFriend>();//!< Liste de relation.


	/***********************************************************************//**
	 * @brief Permet d'envoyer une invitation à un utilisateur
	 * @param[in] from		L'utilisateur qui envoie l'invitation
	 * @param[in] to		L'utilisateur qui reçoit l'invitation
	 * @return TRUE si l'invitation a bien été envoyée. FALSE si from == to !
	 *
	 * @note Si une invitation existe pour from -> to et que to envoie une invitation à from ( to -> from )
	 * alors l'invitation de from est accepté.
	 */
	@Override
	public boolean sendInvitation( User from, User to )
	{
		assert from != null : "{from} ne peut être null !";
		assert to != null : "{to} ne peut être null !";

		if( from.equals(to) )
			return false;

		for( ConnectionFriend cf : m_users ){
			if( cf.m_from.equals(to) && cf.m_to.equals(from) ){
				cf.m_isAccepted = true;
				return true;
			}
			if( cf.m_from.equals(from) && cf.m_to.equals(to) )
				return false;
		}

		return m_users.add(new ConnectionFriend(from, to));
	}


	/***********************************************************************//**
	 * @brief Permet d'accèpter une invitation
	 * @param[in] from		L'utilisateur qui envoie l'invitation
	 * @param[in] to		L'utilisateur qui ACCEPTE l'invitation
	 * @return TRUE Si l'invitation est bien trouvée. FALSE sinon
	 */
	@Override
	public boolean invitationAccepted( User from, User to )
	{
		assert from != null : "{from} ne peut être null !";
		assert to != null : "{to} ne peut être null !";

		for( ConnectionFriend cf : m_users ){
			if( !cf.m_isAccepted && cf.m_from.equals(from) && cf.m_to.equals(to) ){
				cf.m_isAccepted = true;
				return true;
			}
		}
		return false;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste d'amis de {u}
	 * @param[in] u		L'utilisateur qui cherche sa liste d'amis.
	 * @return Une liste d'ami ( user ) (Amitié confirmée !)
	 */
	@Override
	public Set<User> getFriendList( User u )
	{
		assert u != null : "{u} ne peut être null !";

		Set<User> lst = new HashSet<User>();
		for( ConnectionFriend cf : m_users ){
			if( cf.m_isAccepted ){
				if( cf.m_from.equals(u) ){
					lst.add(cf.m_to);
				}else if( cf.m_to.equals(u) ){
					lst.add(cf.m_from);
				}
			}
		}
		return lst;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste des invitations non accèpté envoyée PAR {u}
	 * @param[in] u		L'utilisateur qui cherche a obtenir sa liste d'invitation.
	 * @return Une liste de future amis ? ( user )
	 */
	@Override
	public Set<User> getInvitationSendBy( User u )
	{
		assert u != null : "{u} ne peut être null !";

		Set<User> lst = new HashSet<User>();
		for( ConnectionFriend cf : m_users ){
			if( !cf.m_isAccepted && cf.m_from.equals(u) )
				lst.add(cf.m_to);
		}
		return lst;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste des invitations non accèpté envoyée POUR {u}
	 * @param[in] u		L'utilisateur qui cherche a obtenir sa liste d'invitation.
	 * @return Une liste de future amis ? ( user )
	 */
	@Override
	public Set<User> getInvitationSendFor( User u )
	{
		assert u != null : "{u} ne peut être null !";

		Set<User> lst = new HashSet<User>();
		for( ConnectionFriend cf : m_users ){
			if( !cf.m_isAccepted && cf.m_to.equals(u) )
				lst.add(cf.m_from);
		}
		return lst;
	}


	/***********************************************************************//**
	 * @brief Permet de détermier si {a} est ami avec {b}
	 * @param[in] a		L'utilisateur A
	 * @param[in] b		L'utilisateur B
	 * @return Une liste de user
	 */
	@Override
	public boolean isFriend( User a, User b )
	{
		assert a != null : "{a} ne peut être null !";
		assert b != null : "{b} ne peut être null !";

		for( ConnectionFriend cf : m_users ){
			if( cf.m_isAccepted && ((cf.m_from.equals(a) && cf.m_to.equals(b)) || (cf.m_from.equals(b) && cf.m_to.equals(a))) )
				return true;
		}
		return false;
	}


	/***********************************************************************//**
	 * @brief Permet de SUPPRIMER la relation entre {a} et {b}
	 * @param[in] a		L'utilisateur A
	 * @param[in] b		L'utilisateur B
	 * @return TRUE Si la relation entre {a} et {b} existait
	 */
	@Override
	public boolean remove( User a, User b )
	{
		assert a != null : "{a} ne peut être null !";
		assert b != null : "{b} ne peut être null !";

		for( ConnectionFriend cf : m_users ){
			if( (cf.m_from.equals(a) && cf.m_to.equals(b)) || (cf.m_from.equals(b) && cf.m_to.equals(a)) ){
				m_users.remove(cf);
				return true;
			}
		}
		return false;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le nombre de relations dans toute la base.
	 * (Peut importe les utilisateurs)
	 * @return > 0
	 */
	@Override
	public int size()
	{
		return m_users.size();
	}


	/***********************************************************************//**
	 * @brief Cette fonction n'est là que pour permettre des tests approfondit
	 * sur ConnectionFriend.
	 * @param a		L'ami de {b}
	 * @param b		L'ami de {a}
	 * @param c		L'ami de {d}
	 * @param d		L'ami de {c}
	 * @return TRUE si (a==c && b==d) || (a==d && b==c)
	 * @warning NE PAS UTILISER CETTE FONCTION EN DEHORS DE TEST !
	 */
	public static boolean structTest_equals( User a, User b, User c, User d )
	{
		if( c == null && d == null )
			return new ConnectionFriend(a, b).equals(null);
		return new ConnectionFriend(a, b).equals(new ConnectionFriend(c, d));
	}


	/***********************************************************************//**
	 * @brief Cette fonction n'est là que pour permettre des tests approfondit sur
	 * ConnectionFriend.
	 * @param a		L'ami de {b}
	 * @param b		L'ami de {a}
	 * @return Le hash code de la relation
	 */
	public static int structTest_hashCode( User a, User b )
	{
		return new ConnectionFriend(a, b).hashCode();
	}
}
