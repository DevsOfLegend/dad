package fr.unilim.msi.dad.web.mvc;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/***************************************************************************//**
 * @brief Cette classe implémente un système de login.
 */
@Controller
public class LoginController
{
	@Resource
	private UserDao m_userDao;//!< Liste des user
	@Resource
	private UserSession m_userSession;//!< Le user actuellement logué


	/***********************************************************************//**
	 * @brief Page GET /login.htm
	 * @return La vue correspondante
	 */
	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public ModelAndView getLoginForm()
	{
		return new ModelAndView("login", "user", new User());
	}


	/***********************************************************************//**
	 * @brief Permet de traiter le formulaire de connection
	 * @param user		Les données du formulaire
	 * @param result	Les données en résultat
	 * @return La vue a utiliser / Une redirection
	 */
	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public String postLoginCheck( User user, BindingResult result )
	{
		assert user != null : "{user} ne peut être null !";
		assert result != null : "{result} ne peut être null !";

		// On vérifie que l'association (login, password) existe
		if( m_userDao.getFromUser(user) == null ){
			result.rejectValue("username","login.form.invalid");
			return "login";
		}

		User userFromDao = m_userDao.getFromUsername(user.getUsername());
		m_userSession.setUsername(userFromDao.getUsername());

		return "redirect:/index.htm";
	}


	/***********************************************************************//**
	 * @brief Permet de definir le userDao
	 * @param userDao	Le nouveau userDao
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserDao( UserDao userDao )
	{
		m_userDao = userDao;
	}


	/***********************************************************************//**
	 * @brief Permet de definir le userSession
	 * @param userSession	Le nouveau userSession
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}
}
