package fr.unilim.msi.dad.web.mvc;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

/***************************************************************************//**
 * @brief Classe vérifiant si le destinataire de la demande de relation est bien
 * un utilisateur existant.
 *
 * @note Cette classe implémente également les attributs de modèle :
 * users, friends, userSession, invitationRcv, invitationSent.
 */
@Controller
@SessionAttributes({"ViewUsersController.acceptInvitation","ViewUsersController.removeFriend"})
public class ViewUsersController
{
	@Resource
	private UserDao m_userDao;//!< Liste des utilisateurs
	@Resource
	private UserSession m_userSession;//!< Utilisateur actuellement connecté
	@Resource
	private FriendDao m_friendDao;//!< Liste des amis


	/***********************************************************************//**
	 * @brief Création du modèle caractérisant l'ensemble des utilisateurs
	 * @return L'ensemble des utilisateurs
	 *
	 */
	@ModelAttribute("users")
	public Set<User> populateUsers()
	{
		return m_userDao.getAllUsers();
	}


	/***********************************************************************//**
	 * @brief Création du modèle caractérisant l'ensemble des amis
	 * @return L'ensemble des amis
	 *
	 */
	@ModelAttribute("friends")
	public Set<User> populateFriends()
	{
		return m_friendDao.getFriendList(new User().set(m_userSession.getUsername(),""));
	}


	/***********************************************************************//**
	 * @brief Création du modèle caractérisant l'utilisateur actuel
	 * @return La session actuellement ouverte
	 *
	 */
	@ModelAttribute("userSession")
	public UserSession populateUser()
	{
		return m_userSession;
	}


	/***********************************************************************//**
	 * @brief Création du modèle caractérisant les utilisateurs ayant demandé
	 * une relation d'ami avec l'utilisateur courant.
	 * @return L'ensemble des utilisateurs ayant demandé une relation d'ami
	 *
	 */
	@ModelAttribute("invitationsRcv")
	public Set<User> populateInvitationsRcv()
	{
		return m_friendDao.getInvitationSendFor(m_userDao.getFromUsername(m_userSession.getUsername()));
	}


	/***********************************************************************//**
	 * @brief Création du modèle caractérisant les utilisateurs ayant demandé une
	 * relation d'ami avec l'utilisateur courant.
	 * @return L'ensemble des utilisateurs ayant demandé une relation d'ami
	 *
	 * @bug Cette fonction ne semble pas fonctionner...
	 */
	@ModelAttribute("invitationsSent")
	public Set<User> populateInvitationsSent()
	{
		return m_friendDao.getInvitationSendBy(m_userDao.getFromUsername(m_userSession.getUsername()));
	}


	/***********************************************************************//**
	 * @brief Permet de faire l'association antre l'URL entrée et la page JSP à afficher
	 * @return La vue a utiliser / Une redirection
	 *
	 */
	@RequestMapping(value={"/index.htm"},method=RequestMethod.GET)
	public String getViewUsers()
	{
		return "viewUsers";
	}


	/***********************************************************************//**
	 * @brief Permet d'accepter une invitation
	 * @return La vue a utiliser / Une redirection
	 *
	 */
	@RequestMapping(value={"/accept-{username}.htm"},method=RequestMethod.GET)
	public String acceptInvitation( ModelMap model, @PathVariable(value = "username") String username )
	{
		assert model != null : "{model} ne peut être null !";
		assert username != null : "{username} ne peut être null !";

		User u = m_userDao.getFromUsername(username);
		if( u == null ){
			// L'utilisateur n'est pas dans la base de donnée.
			model.put("ViewUsersController.acceptInvitation", "notFound");
		}else{
			// L'utilisateur est dans la base.
			if( m_friendDao.invitationAccepted(u, new User().set(m_userSession.getUsername(),null)) ){
				// Relation confirmée
				model.put("ViewUsersController.acceptInvitation", "OK");
			}else{
				// Relation non trouvée
				model.put("ViewUsersController.acceptInvitation", "noRelationFound");
			}
		}

		return "redirect:/index.htm";
	}


	/***********************************************************************//**
	 * @brief Permet de supprimer une relation d'amitié
	 * @return La vue à utiliser / Une redirection
	 *
	 */
	@RequestMapping(value={"/delete-{username}.htm"},method=RequestMethod.GET)
	public String removeFriend( ModelMap model, @PathVariable(value = "username") String username )
	{
		assert model != null : "{model} ne peut être null !";
		assert username != null : "{username} ne peut être null !";

		User u = m_userDao.getFromUsername(username);
		if( u == null ){
			// L'utilisateur n'est pas dans la base de donnée.
			model.put("ViewUsersController.removeFriend", "notFound");
		}else{
			// L'utilisateur est dans la base.
			if( m_friendDao.isFriend(u, new User().set(m_userSession.getUsername(),null)) ){
				// Relation confirmée
				m_friendDao.remove(u, new User().set(m_userSession.getUsername(),null));
				model.put("ViewUsersController.removeFriend", "OK");
			}else{
				// Relation non trouvée
				model.put("ViewUsersController.removeFriend", "noRelationFound");
			}
		}
		return "redirect:/index.htm";
	}


	/***********************************************************************//**
	 * @brief Initialisation de l'ensemble des utilisateurs
	 * @param[in,out] userDao ensemble des utilisateurs
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserDao( UserDao userDao )
	{
		m_userDao = userDao;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le UserDao
	 * @return Le UserDao actuel.
	 * @note Utilisé pour les JTests
	 */
	public final UserDao getUserDao()
	{
		return m_userDao;
	}


	/***********************************************************************//**
	 * @brief Initialisation de l'ensemble des amis
	 * @param[in,out] friendDao	Ensemble des amis
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setFriendDao( FriendDao friendDao )
	{
		m_friendDao = friendDao;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le FriendDao
	 * @return Le FriendDao actuel
	 * @note Utilisé pour les JTests
	 */
	public final FriendDao getFriendDao()
	{
		return m_friendDao;
	}


	/***********************************************************************//**
	 * @brief Initialisation de la session en cours avec l'utilisateur en cours
	 * @param[in,out] userSession	Contient les informations de la session en cours
	 * @return[NONE]
	 *
	 * @note Utilisé par Spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le UserSession
	 * @return Le UserSession actuel
	 * @note Utilisé pour les JTests
	 */
	public final UserSession getUserSession()
	{
		return m_userSession;
	}
}
