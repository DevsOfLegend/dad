package fr.unilim.msi.dad.web.mvc;

import java.util.Set;

/***************************************************************************//**
 * @brief Permet de manipuler une liste d'ami indépendante de l'utilisateur.
 */
public interface FriendDao
{
	/***********************************************************************//**
	 * @brief Permet d'envoyer une invitation à un utilisateur
	 * @param[in] from		L'utilisateur qui envoie l'invitation
	 * @param[in] to		L'utilisateur qui reçoit l'invitation
	 * @return TRUE si l'invitation a bien été envoyée. FALSE si from == to !
	 *
	 * @note Si une invitation existe pour from -> to et que to envoie une invitation à from ( to -> from )
	 * alors l'invitation de from est accèpté.
	 */
	public boolean sendInvitation( User from, User to );


	/***********************************************************************//**
	 * @brief Permet d'accepter une invitation
	 * @param[in] from		L'utilisateur qui envoie l'invitation
	 * @param[in] to		L'utilisateur qui ACCEPTE l'invitation
	 * @return TRUE Si l'invitation est bien trouvée. FALSE sinon
	 */
	public boolean invitationAccepted( User from, User to );


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste d'amis de {u}
	 * @param[in] u		L'utilisateur qui cherche sa liste d'amis.
	 * @return Une liste d'ami ( user ) (Amitié confirmée !)
	 */
	public Set<User> getFriendList( User u );


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste des invitations non accèpté envoyée PAR {u}
	 * @param[in] u		L'utilisateur qui cherche a obtenir sa liste d'invitation
	 * @return Une liste de future amis ? ( user )
	 */
	public Set<User> getInvitationSendBy( User u );


	/***********************************************************************//**
	 * @brief Permet d'obtenir la liste des invitations non accèpté envoyée POUR {u}
	 * @param[in] u		L'utilisateur qui cherche a obtenir sa liste d'invitation
	 * @return Une liste de future amis ? ( user )
	 */
	public Set<User> getInvitationSendFor( User u );


	/***********************************************************************//**
	 * @brief Permet de déterminer si {a} est ami avec {b}
	 * @param[in] a		L'utilisateur A
	 * @param[in] b		L'utilisateur B
	 * @return Une liste de user
	 */
	public boolean isFriend( User a, User b );


	/***********************************************************************//**
	 * @brief Permet de SUPPRIMER la relation entre {a} et {b}
	 * @param[in] a		L'utilisateur A
	 * @param[in] b		L'utilisateur B
	 * @return TRUE Si la relation entre {a} et {b} existait
	 */
	public boolean remove( User a, User b );


	/***********************************************************************//**
	 * @brief Permet d'obtenir le nombre de relations dans toute la base.
	 * (Peut importe les utilisateurs)
	 * @return > 0
	 */
	public int size();
}
