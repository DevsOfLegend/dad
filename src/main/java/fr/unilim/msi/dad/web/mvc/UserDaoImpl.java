package fr.unilim.msi.dad.web.mvc;

import java.util.HashSet;
import java.util.Set;
import org.springframework.stereotype.Repository;

/***************************************************************************//**
 * @brief Cette classe permet de manipuler une liste d'utilisateur
 */
@Repository(value = "userDao")
public class UserDaoImpl implements UserDao
{
	private Set<User> m_users = new HashSet<User>();//!< Liste des utilisateurs. Le username est UNIQUE


	/***********************************************************************//**
	 * @brief Initialisation du gestionnaire d'utilisateur.
	 * @return[NONE]
	 * @note C'est ici que l'on initialise les utilisateurs.
	 */
	public UserDaoImpl()
	{
		m_users.add(new User().set("admin","admin"));
		m_users.add(new User().set("toto","toto"));
	}


	/***********************************************************************//**
	 * @brief Permet d'enregistrer un nouvel utilisateur.
	 * @param[in] user	Le nouveau user a enregistrer
	 * @return FALSE Si l'utilisateur est déjà dans la liste. TRUE Sinon.
	 */
	@Override
	public boolean save( User user )
	{
		assert user != null : "User can not be null !";

		if( m_users.contains(user) )
			return false;

		m_users.add(user);
		return true;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir un utilisateur en fonction de son nom.
	 * @param[in] username	Le nom de l'utilisateur a chercher
	 * @return NULL si l'utilisateur n'est pas trouvé. NOT NULL sinon
	 */
	@Override
	public User getFromUsername( String username )
	{
		assert username != null : "Username ne peut être null.";

		for( User user : m_users ){
			if( user.getUsername().equals(username) )
				return user;
		}
		return null;
	}


	/***********************************************************************//**
	 * @brief Permet de savoir si un utilisateur ( +password ) est dans la liste.
	 * @param[in] user	L'utilisateur a tester
	 * @return NULL si l'utilisateur n'est pas trouvé. {user} sinon
	 */
	@Override
	public User getFromUser( User user )
	{
		assert user != null : "Username ne peut être null.";

		for( User user_tmp : m_users ){
			if (user_tmp.getUsername().equals(user.getUsername())
					&&
				user_tmp.getPassword().equals(user.getPassword()))
			{
				return user;
			}
		}
		return null;
	}


	/***********************************************************************//**
	 * @brief Permet d'accéder directement à la liste des utilisateurs
	 * @return La liste des utilisateurs
	 */
	@Override
	public Set<User> getAllUsers()
	{
		return m_users;
	}
}
