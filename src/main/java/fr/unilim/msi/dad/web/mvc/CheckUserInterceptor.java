package fr.unilim.msi.dad.web.mvc;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/***************************************************************************//**
 * @brief C'est dans cette class que l'on lie le système de login et le reste du système.
 */
public class CheckUserInterceptor extends HandlerInterceptorAdapter
{
	@Resource
	private UserSession m_userSession;//!< L'utilisateur actuellement logué


	/***********************************************************************//**
	 * @brief C'est dans cette fonctoin que l'on check si l'on est bien connecté.
	 * @param request		Info sur la requette
	 * @param response		Header pour la réponse
	 * @param handler		...
	 * @return TRUE si on est logué ou si l'on se trouve sur login.htm. FALSE sinon
	 * @throws IOException	En cas de problème lors de la redirection
	 */
	@Override
	public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException
	{
		if( request.getServletPath().equals("/login.htm") )
			return true;

		String username = m_userSession.getUsername();

		if( username != null )
			return true;

		response.sendRedirect("login.htm");
		return false;
	}

	/***********************************************************************//**
	 * @brief Permet de set userSession
	 * @param userSession	La nouvelle valeur de userSession
	 * @return[NONE]
	 *
	 * @note Utilisé par spring -> Resource
	 */
	public void setUserSession( UserSession userSession )
	{
		m_userSession = userSession;
	}
}
