package fr.unilim.msi.dad.web.mvc;


/***************************************************************************//**
 * @brief Permet de gérer un utilisateur
 */
public class User
{
	private String m_username;//!< Nom de l'utilisateur
	private String m_password;//!< Mot de passe NON CRYPTÉ

	public User() {}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le nom de l'utilisateur
	 * @return Renvoie le nom de l'utilisateur
	 */
	public String getUsername()
	{
		return m_username;
	}


	/***********************************************************************//**
	 * @brief Permet de changer le nom de l'utilisateur
	 * @param[in] username		Le nouveau nom d'utilisateur
	 * @return[NONE]
	 * @warning Le nom d'utilisateur ne peut être null !
	 */
	public void setUsername( String username )
	{
		assert username != null : "Le nom d'utilisateur ne peut être null !";
		m_username = username;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir le mot de passe
	 * @return Le mot de passe actuel
	 */
	public String getPassword()
	{
		return m_password;
	}


	/***********************************************************************//**
	 * @brief Permet de changer le mot de passe
	 * @param[in] password	Le nouveau mot de passe
	 * @return[NONE]
	 */
	public void setPassword( String password )
	{
		m_password = password;
	}


	/***********************************************************************//**
	 * @brief Permet de changer le nom d'utilisateur {username} et le mot
	 * de passe {password}.
	 * @param[in] username	Le nom de l'utilisateur.
	 * @param[in] password	Le mot de passe de l'utilisateur.
	 * @return L'instance de la class
	 * @note En temps normal cette methode devrait être le constructeur:
	 * User(String,String)
	 * Mais Java traite les objets et leurs héritages dans un ordre qui fait
	 * qu'appeler une methode interne dans le constructeur risque d'engendrer des problèmes.
	 * Pour garder la possibilité d'hériter de User, nous avons donc conçut cette methode
	 * "batarde".
	 * Pour en savoir plus:
	 * http://stackoverflow.com/questions/3404301/whats-wrong-with-overridable-method-calls-in-constructors
	 *
	 * Exemple d'utilisation:
	 * @code
	 * // Si on utilise set:
	 * User u = new User().set("user","badpass");
	 *
	 * // Sans set:
	 * User u = new User();
	 * u.setUsername("user");
	 * u.setPassword("badpass");
	 * @endcode
	 */
	public User set( String username, String password )
	{
		setUsername(username);
		setPassword(password);
		return this;
	}


	/***********************************************************************//**
	 * @brief Permet de tester l'egalité entre 2 utilisateurs
	 * @param[in] obj L'utilisateur a comparer avec celui-ci
	 * @return TRUE si les 2 users sont identiques. FALSE sinon
	 */
	@Override
	public boolean equals( Object obj )
	{
		if( obj == null )
			return false;

		if( getClass() != obj.getClass() )
			return false;

		final User other = (User) obj;

		if ((this.m_username == null) ? (other.m_username != null) : !this.m_username.equals(other.m_username))
			return false;

		return true;
	}


	/***********************************************************************//**
	 * @brief Permet d'obtenir un hash du nom de l'utilisateur
	 * @return Le hash du username
	 * @note Le hash est une valeur numéric >= 29*7
	 */
	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 29 * hash + (this.m_username != null ? this.m_username.hashCode() : 0);
		return hash;
	}
}
