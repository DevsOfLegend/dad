package fr.unilim.msi.dad.web.mvc;

import java.util.Set;


/***************************************************************************//**
 * @brief Ensemble des utilisateurs existants
 *
 */
public interface UserDao
{
	/***********************************************************************//**
	 * @brief Permet d'ajouter un utilisateur à l'ensemble des utilisateurs
	 * @param[in] user	Utilisateur à ajouter
	 * @return TRUE si le user a été enregistré. FALSE sinon
	 *
	 */
	boolean save( User user );


	/***********************************************************************//**
	 * @brief Permet de retrouver un utilisateur à partir de son login
	 * @param[in] username	Username à parti duquel retrouver l'utilisateur.
	 * @return L'utilisateur caractérisé par le mot de passe demandé.
	 *
	 */
	User getFromUsername( String username );


	/***********************************************************************//**
	 * @brief Permet de savoir si un utilisateur existe
	 * @param[in] user	user à rechercer
	 * @return Retourne l'utilisateur demandé
	 *
	 */
	User getFromUser(User user);


	/***********************************************************************//**
	 * @brief Renvoie l'ensemble de tous les utilisateurs existants
	 * @return L'ensemble des utilisateurs
	 *
	 */
	Set<User> getAllUsers();
}
