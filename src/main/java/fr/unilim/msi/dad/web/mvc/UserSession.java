package fr.unilim.msi.dad.web.mvc;

import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/***************************************************************************//**
 * @brief Classe possédant les informations sur la session en cours
 *
 */
@Component
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSession implements Serializable
{
	private String m_username;//!< Utilisateur actuellement logué

	/***********************************************************************//**
	 * @brief Renvoie le login de l'utilisateur
	 * @return Le username de l'utilisateur
	 */
	public String getUsername()
	{
		return m_username;
	}


	/***********************************************************************//**
	 * @brief Modifie le login de l'utilisateur
	 * @param[in] username		Le nom du user actuellement logué
	 * @return [NONE]
	 */
	public void setUsername( String username )
	{
		m_username = username;
	}
}
