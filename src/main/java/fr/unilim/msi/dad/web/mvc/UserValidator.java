package fr.unilim.msi.dad.web.mvc;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/***************************************************************************//**
 * @brief Classe vérifiant la validité d'un login et d'un mot de passe
 *
 * @note Les login/password doivent dépassés le s3 lettres
 */
@Component
public class UserValidator implements Validator
{
	/***********************************************************************//**
	 * @brief Système de comparaison pour Validator
	 * @param clazz	...
	 * @return	...
	 */
	@Override
	public boolean supports( Class<?> clazz )
	{
		return User.class.equals(clazz);
	}


	/***********************************************************************//**
	 * @brief Vérifie la validité des login et mot de passe choisit par le nouvel
	 * utilisateur.
	 * @param[in] target	L'utilisateur a tester
	 * @param[out] errors	Les érreurs émises.
	 * @return[NONE]
	 *
	 * @note Les login/password doivent dépassés les 3 lettres
	 */
	@Override
	public void validate( Object target, Errors errors )
	{
		User user = (User)target;

		if( user.getUsername().length() < 3 )
			errors.rejectValue("username", "validator.user.username.minimal.size");

		if( user.getPassword().length() < 3 )
			errors.rejectValue("password", "validator.user.password.minimal.size");
	}
}
