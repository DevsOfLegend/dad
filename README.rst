======
README
======

:Info: See <https://bitbucket.org/DevsOfLegend/dad/> for repo.
:Author: NUEL Guillaume (ImmortalPC) and DECK Léa (dla)
:Date: $Date: 2013-01-10 $
:Revision: $Revision: 68 $

Application & Test Spring MVC
=============================

Description
-----------

Ce programme met en place une application basée sur Spring MVC ainsi que les tests adéquats.
Ce programme est codé en Java: Java EE ( Servlet, JSP, JSTL

Le système est muni d'un système d'authentification (login/password).
La base de données est composée de l'ensemble des utilisateurs ainsi que l'ensemble des amis.
La relation d'amitié est créée de la manière suivante :

- un utilisateur choisit un utilisateur à ajouter en tant qu'ami
- une demande est envoyée à l'utilisateur concerné (s'il existe)
- le destinataire accèpte ou refuse.
- dans le cas où le destinataire accepte, l'envoyeur obtient une personne en plus dans ses amis.


Matériel utilisé
----------------

**IDE** : Netbeans (Tab réel en taille 4)

**Serveur** : Tomcat, Java7, Maeven


Documentation
-------------

Le code est entièrement documenté d'après les normes doxygen :

- Pour générer les pages html de la documentation Doxygen, éxécuter (au niveau de la racine du projet) : 

>>>	Projet://>	./doxygen


Ce document est (README.rst) est au format RST, ce qui signifit qu'il est possible de générer un PDF ou bien un .HTML à partir de ce document:

- Pour générer un .html à partir du README.rst

>>>	Projet://>	rst2html README.rst README.html


- Pour générer un .pdf à partir du README.rst

>>>	Projet://>	rst2pdf README.rst -o README.pdf