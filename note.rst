I - Première étape: étude de Spring MVC Sample
==============================================

1) Quel est le login et le password de l'utilisateur pour se connecter ?
------------------------------------------------------------------------------
 - **User**:*admin*
 - **Password**:`admin`

| (voir la classe *UserDaoImpl*)
| **NOTE** : Il est à noter que le mot de passe est optionnel :p
| En effet rien dans LoginController.postLoginCheck() ne vérifie le mot de passe.
|

2) Quelle est la classe en charge de gérer l'authentification ?
---------------------------------------------------------------
| La classe **LoginController.postLoginCheck()** s'occupe, pour l'instant, de vérifier si le login entré est bien présent dans la base de donnée.
| **ATTENTION** : il ne vérifie pas si le password est correct.
|

3) Comment sont vérifier les entrées lors de l'ajout d'un utilisateur ?
--------------------------------------------------------------------------------
| Les entrées sont traitées par la fonction **NewUserController.postNewUser()**.
| Ces entrées sont vérifiées dans la fonction **UserValidator.validate()** :

- le login et le password du nouvel utilisateur doivent dépasser les deux lettres.
- si le login existe déjà on lève une erreur.
- sinon on enregistre le nouvel utilisateur.

|

II - Deuxième étape: découverte
===============================

1) Réparer le système de login (pour l'instant, n'mporte quel password est autorisé)
-----------------------------------------------------------------------------------------
 **1**- ajout de la fonction **getFromUser()** à l'interface **UserDAO** 
 **2**- implémentation de cette fonction dans la classe **UserDAOImpl**
 **3**- on remplace la fonction **getFromUsername()** par **getFromUser()** dans **LoginController.postLoginCheck()**

| **NOTE** : désormais on vérifie l'existence de l'association (login, password) et non plus que l'existence du login.

|

2) Ajouter un système d'amis:
-----------------------------
**A FAIRE** :

- chaque utilisateur peut demander alors à un autre utilisateur d'être son ami
- si celui ci accepte alors les deux utilisateurs voient apparaître l'autre personne comme amis

**DESCRIPTION DU CODE** :

- ajout de l'interface **FriendDao**, avec les fonctions suivantes : 
	 - **sendInvitation()** (envoie d'une demande d'ami)
	 - **invitationAccepted()** (permet au destinataire d'acceptre l'invitation)
	 - **getFriendList()** (renvoie la liste d'ami de l'utilisateur)
	 - **getInvitationSendBy()** (renvoie la liste des invitation non encore acceptée venant de l'utilisateur)
	 - **isFriend()** (test s'il y a une relation d'amitié entre les deux utilisateurs)
	 - **remove()** (suppression d'une relation d'amitié)
- ajout de la classe **FriendDaoImpl** implémentant l'interface **FriendDao**
- ajout de la classe **NewFriendController** controlant l'ajout d'un nouvel ami
- ajout du fichier JSP associé : **newFriend.jsp**
- modification du fichier **index.jsp** avec l'ajout du lien vers la page **newFriend.jsp**
- BEAN ???????????????????????????????

|

III - Troisième étape: Projet
=============================

1) Le projet doit avoir deux DAO(s)
-----------------------------------
**A FAIRE** :
 - Le premier pour l'accès à User et le second pour l'accès aux liaisons Amis.
 - Chaque DAO doit avoir les méthodes pour accéder et créer les éléments (les méthodes update et delete n'ont dans ce cadre peu de sens).

**DESCRIPTION DU CODE** :
 - voir partie précédente.
 - les deux DAO sont désormais les suivants : **UserDao** & **FriendDao** (toutes deux des interfaces)
 - méthodes d'accès pour **FriendDao** : les fonctions décrite en 2) de la deuxième étape du projet. 
 - méthodes d'accès pour **UserDao** : 
	- **save()** (permet d'ajouter un utilisateur à condition que le login entré n'existe pas encore)
	- **getFromUsername()** (permet d'obtenir l'utilisateur, le couple (login, password) correspondant au login entré)
	- **getFromUser()** (permet de vérifier si le couple (login,password) existe : utilisé pour l'authentification)
	- **getAllUsers()** (renvoie un ensemble contenant tous les utilisateurs créés)

|

2) Services
-----------
**A fAIRE** :
 - Insérer une classe pour gérer un service d'ajout d'amis.
 - C'est un bean avec une interface proposant la création d'un ami, la confirmation de l'autre côté et de trouver si deux personnes sont amis et retourner la structure correspondante.

**DESCRIPTION DU CODE** :
 - .?????????????????

|

3) Tests unitaires
------------------

**A FAIRE (NOTE)**:
 - Vous devrez écrire l'ensemble des tests unitaires couvrant votre code. 
 - Vous pouvez lancer le calcul de couverture de votre code avec le goal site.
	- bouton droit sur *projet > goals > custom*
	- entrer dans la boîte goal: *site:site*

|

**DESCRIPTION DU CODE** :
 - .?????????????????

|